<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//Samba-Team//DTD DocBook V4.2-Based Variant V1.0//EN" "http://www.samba.org/samba/DTD/samba-doc">
<refentry id="ctdbd.1">

<refmeta>
	<refentrytitle>ctdbd</refentrytitle>
	<manvolnum>1</manvolnum>
</refmeta>


<refnamediv>
	<refname>ctdbd</refname>
	<refpurpose>The CTDB cluster daemon</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis>
		<command>ctdbd</command>
	</cmdsynopsis>
	
	<cmdsynopsis>
		<command>ctdbd</command>
		<arg choice="req">--reclock=&lt;filename&gt;</arg>
		<arg choice="req">--nlist=&lt;filename&gt;</arg>
		<arg choice="req">--dbdir=&lt;directory&gt;</arg>
		<arg choice="opt">-? --help</arg>
		<arg choice="opt">--usage</arg>
		<arg choice="opt">-i --interactive</arg>
		<arg choice="opt">--public-addresses=&lt;filename&gt;</arg>
		<arg choice="opt">--public-interface=&lt;interface&gt;</arg>
		<arg choice="opt">--event-script=&lt;filename&gt;</arg>
		<arg choice="opt">--logfile=&lt;filename&gt;</arg>
		<arg choice="opt">--listen=&lt;address&gt;</arg>
		<arg choice="opt">--transport=&lt;STRING&gt;</arg>
		<arg choice="opt">--socket=&lt;filename&gt;</arg>
		<arg choice="opt">-d --debug=&lt;INTEGER&gt;</arg>
		<arg choice="opt">--torture</arg>
	</cmdsynopsis>
	
</refsynopsisdiv>

  <refsect1><title>DESCRIPTION</title>
    <para>
      ctdbd is the main ctdb daemon.
    </para>
    <para>
      ctdbd provides a clustered version of the TDB database with automatic rebuild/recovery of the databases upon nodefailures.
    </para>
    <para>
      Combined with a cluster filesystem ctdbd provides a full HA environment for services such as clustered Samba and NFS as well as other services.
    </para>
    <para>
      ctdbd provides monitoring of all nodes in the cluster and automatically reconfigures the cluster and recovers upon node failures.
    </para>
    <para>
      ctdbd is the main component in clustered Samba that provides a high-awailability load-sharing CIFS server cluster.
    </para>
  </refsect1>


  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry><term>-? --help</term>
        <listitem>
          <para>
            Print some help text to the screen.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--usage</term>
        <listitem>
          <para>
            Print useage information to the screen.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--reclock=&lt;filename&gt;</term>
        <listitem>
          <para>
            This is the name of the lock file stored of the shared cluster filesystem that ctdbd uses to arbitrate which node has the role of recovery-master.
            This file must be stored on shared storage.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--nlist=&lt;filename&gt;</term>
        <listitem>
          <para>
            This file contains a list of the private ip addresses of every node in the cluster. There is one line/ip address for each node. This file must be the same for all nodes in the cluster.
          </para>
          <para>
            This file is usually /etc/ctdb/nodes .
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--dbdir=&lt;directory&gt;</term>
        <listitem>
          <para>
            This is the directory on local storage where ctdbd keeps the local
            copy of the TDB databases. This directory is local for each node and should not be stored on the shared cluster filesystem.
          </para>
          <para>
            This directory would usually be /var/ctdb .
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-i --interactive</term>
        <listitem>
          <para>
            By default ctdbd will detach itself from the shell and run in
            the background as a daemon. This option makes ctdbd to start in interactive mode.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--public_addresses=&lt;filename&gt;</term>
        <listitem>
          <para>
            When used with IP takeover this specifies a file containing the public ip addresses to use on the cluster. This file contains one entry for each node in the cluster.
          </para>
          <para>
            This is usually the file /etc/ctdb/public_addresses
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--public-interface=&lt;interface&gt;</term>
        <listitem>
          <para>
            When used with IP takeover this option specifies which physical interface should be used to attach the public addresses to.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--event-script=&lt;filename&gt;</term>
        <listitem>
          <para>
            This option is used to specify which events script that ctdbd will
            use to manage services when the cluster configuration changes.
          </para>
          <para>
            This will normally be /etc/ctdb/events which is part of the ctdb distribution.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--logfile=&lt;filename&gt;</term>
        <listitem>
          <para>
            This is the file where ctdbd will write its log. This is usually /var/log/log.ctdb .
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--listen=&lt;address&gt;</term>
        <listitem>
          <para>
            This specifies which ip address ctdb will bind to. By default ctdbd will bind to the first address it finds in the /etc/ctdb/nodes file and which is also present on the local system in which case you do not need to provide this option.
          </para>
          <para>
            This option is only required when you want to run multiple ctdbd daemons/nodes on the same physical host in which case there would be multiple entries in /etc/ctdb/nodes what would match a local interface.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--transport=&lt;STRING&gt;</term>
        <listitem>
          <para>
            This option specifies which transport to use for ctdbd internode communications. The default is "tcp".
          </para>
          <para>
            Suported transports are "tcp" and "infiniband".
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--socket=&lt;filename&gt;</term>
        <listitem>
          <para>
            This specifies the name of the domain socket that ctdbd will create. This socket is used for local clients to attach to and communicate with the ctdbd daemon.
          </para>
          <para>
            The default is /tmp/ctdb.socket . You only need to use this option if you plan to run multiple ctdbd daemons on the same physical host.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>-d --debug=&lt;DEBUGLEVEL&gt;</term>
        <listitem>
          <para>
            This option sets the debuglevel on the ctdbd daemon which controls what will be written to the logfile. The default is 0 which will only log important events and errors. A larger number will provide additional logging.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry><term>--torture</term>
        <listitem>
          <para>
            This option is only used for development and testing of ctdbd. It adds artificial errors and failures to the common codepaths in ctdbd to verify that ctdbd can recover correctly for failures.
          </para>
          <para>
            You do NOT want to use this option unless you are developing and testing new functionality in ctdbd.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>


  <refsect1><title>Private vs Public addresses</title>
    <para>
      When used for ip takeover in a HA environment, each node in a ctdb 
      cluster has two ip addresses assigned to it. One private and one public.
    </para>

    <refsect2><title>Private address</title>
      <para>
        This is the physical ip address of the node which is configured in 
        linux and attached to a physical interface. This address uniquely
        identifies a physical node in the cluster and is the ip addresses
        that ctdbd will use to communicate with the ctdbd daemons on the
        other nodes in the cluster.
      </para>
      <para>
        The private addresses are configured in /etc/ctdb/nodes 
        (unless the --nlist option is used) and contain one line for each 
        node in the cluster. Each line contains the private ip address for one
        node in the cluster.
      </para>
      <para>
        Each node is assigned an internal node number which corresponds to 
        which line in the nodes file that has the local private address
        of the node.
      </para>
      <para>
        Since the private addresses are only available to the network when the
        corresponding node is up and running you should not use these addresses
        for clients to connect to services provided by the cluster. Instead
        client applications should only attach to the public addresses since
        these are guaranteed to always be available.
      </para>
      Example /etc/ctdb/nodes for a four node cluster:
      <screen format="linespecific">
        10.1.1.1
        10.1.1.2
        10.1.1.3
        10.1.1.4
      </screen>
    </refsect2>
    <refsect2><title>Public address</title>
      <para>
        A public address on the other hand is not attached to an interface.
        This address is managed by ctdbd itself and is attached/detached to
        a physical node at runtime. You should NOT have this address configured
        to an interface in linux. Let ctdbd manage these addresses.
      </para>
      <para>
        The ctdb cluster will assign/reassign these public addresses across the
        available nodes in the cluster. When one node fails, its public address
        will be migrated to and taken over by a different node in the cluster
        to ensure that all public addresses are always available to clients.
      </para>
      <para>
        These addresses are not physically attached to a specific node. 
        The 'ctdb ip' command can be used to view the current assignment of 
        public addresses and which physical node is currently serving it.
      </para>
      <para>
        By default, each node will when operational always serve its primary 
        public address which is the corresponding line for that node number 
        in the public addresses file. I.e. as long as node X is available and
        fully oprational it will always be the node that serves the 
        corresponding public address.
      </para>
      <para>
        The list of public addresses also contain the netmask for that address.
        the reason for this is because ctdbd needs to know which mask to use
        when it adds/removes the address from a physical node. This netmask
        is also used by ctdbd when making decisions on which node should take
        over a public ip address for a failed node.
        A node will only be allowed to take over a public address from a
        different node IFF that public address resides in the same subnet
        as the primary public address for that node.
      </para>
      Example /etc/ctdb/public_addresses for a four node cluster:
      <screen format="linespecific">
        11.1.1.1/24
        11.1.1.2/24
        11.1.2.1/24
        11.1.2.2/24
      </screen>
      <para>
        In this example, if node 3 fails, its public address can be taken over 
        by node 2 since node 2 is on the same subnet as 3 but not by node 0 or 
        node 1 since node 0 and 1 are both on a different subnet from node 3.
      </para>
    </refsect2>
  </refsect1>


  <refsect1><title>Node status</title>
    <para>
      The current status of each node in the cluster can be viewed by the 
      'ctdb status' command.
    </para>
    <para>
      There are five possible for a node.
    </para>

    <para>
      OK - This node is fully functional.
    </para>
    
    <para>
      DISCONNECTED - This node could not be connected through the network 
      and is currently not parcipitating in the cluster. If there is a 
      public IP address associated with this node it should have been taken 
      over by a different node. No services are running on this node.
    </para>
 
    <para>
      DISABLED - This node has been administratively disabled. This node is 
      still functional and participates in the CTDB cluster but its IP 
      addresses have been taken over by a different node and no services are 
      currently being hosted.
    </para>
   
    <para>
      UNHEALTHY - A service provided by this node is malfunctioning and should 
      be investigated. The CTDB daemon itself is operational and participates 
      in the cluster. Its public IP address has been taken over by a different 
      node and no services are currently being hosted. All unhealthy nodes 
      should be investigated and require an administrative action to rectify.
    </para>
    
    <para>
      BANNED - This node failed too many recovery attempts and has been banned 
      from participating in the cluster for a period of RecoveryBanPeriod 
      seconds. Any public IP address has been taken over by other nodes. This 
      node does not provide any services. All banned nodes should be 
      investigated and require an administrative action to rectify. This node 
      does not perticipate in the CTDB cluster but can still be communicated 
      with. I.e. ctdb commands can be sent to it.
    </para>
  </refsect1>


  <refsect1><title>SEE ALSO</title>
    <para>
      ctdb(1), onnode(1)
      <ulink url="http://ctdb.samba.org/"/>
    </para>
  </refsect1>
  <refsect1><title>COPYRIGHT/LICENSE</title>
<literallayout>
Copyright (C) Andrew Tridgell 2007
Copyright (C) Ronnie sahlberg 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see http://www.gnu.org/licenses/.
</literallayout>
  </refsect1>
</refentry>
